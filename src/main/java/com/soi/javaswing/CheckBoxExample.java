/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class CheckBoxExample {

    CheckBoxExample() {
        JFrame f = new JFrame("Check Box Example");
        JCheckBox cBox1 = new JCheckBox("C++");
        cBox1.setBounds(100, 100, 50, 50);
        JCheckBox cBox2 = new JCheckBox("Java", true);
        cBox2.setBounds(100, 150, 70, 50);
        f.add(cBox1);
        f.add(cBox2);
        f.setSize(400, 400);        
        f.setLayout(null);        
        f.setVisible(true);        
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }    

    public static void main(String[] args) {
        new CheckBoxExample();
    }
    
}
