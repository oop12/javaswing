/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class ButtonExample1 {

    public static void main(String[] args) {
        JFrame f = new JFrame("ButtonExample1");

        final JTextField txt = new JTextField();
        txt.setBounds(50, 50, 150, 20);

        JButton btn = new JButton("Click Click");
        btn.setBounds(50, 100, 95, 30);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txt.setText("Welcome to the world");
            }
        });
        f.add(btn);
        f.add(txt);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
