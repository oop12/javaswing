/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
class MyJComponentExample extends JComponent {

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.ORANGE);
        g.fillRect(30, 30, 100, 100);
    }
}

public class JComponentExample {

    public static void main(String[] args) {
        MyJComponentExample com = new MyJComponentExample();
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(com);
        frame.setVisible(true);
    }
}
