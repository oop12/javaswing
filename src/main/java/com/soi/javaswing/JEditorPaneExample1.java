/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class JEditorPaneExample1 {

    JFrame myFrame = null;

    public static void main(String[] a) {
        (new JEditorPaneExample1()).test();
    }

    private void test() {
        myFrame = new JFrame("JEditorPane Test");

        JEditorPane myPane = new JEditorPane();
        myPane.setContentType("text/html");
        myPane.setText("<h1>Sleeping</h1><p>Sleeping is necessary for a healthy body."
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days.</p>");

        myFrame.setContentPane(myPane);
        myFrame.setSize(400, 200);
        myFrame.setVisible(true);
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
