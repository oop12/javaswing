/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JFrame;
import javax.swing.JScrollBar;

/**
 *
 * @author user
 */
public class ScrollBarExample extends JFrame {
    
    ScrollBarExample() {
        new JFrame("ScrollBar Example");
        JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        add(s);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        new ScrollBarExample();
    }
    
}
