/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author user
 */
public class JDPaneDemo extends JFrame {

    public JDPaneDemo() {
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);
        setTitle("JDesktopPane Example");
        setSize(300, 500);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    class CustomDesktopPane extends JDesktopPane {

        int numFrames = 4, x = 30, y = 30;

        public void display(CustomDesktopPane dp) {
            for (int i = 0; i < numFrames; ++i) {
                JInternalFrame jframe = new JInternalFrame("Internal Frame " + i, true, true, true, true);

                jframe.setBounds(x, y, 250, 85);
                Container c1 = jframe.getContentPane();
                c1.add(new JLabel("I love my babe!!!"));
                dp.add(jframe);
                jframe.setVisible(true);
                y += 85;
            }
        }
    }

    public static void main(String[] args) {
        new JDPaneDemo();
    }

}
