/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author user
 */
public class SpinnerExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Spinner Example");
        SpinnerModel value = new SpinnerNumberModel(5, 0, 10, 1);

        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 30);
        f.add(spinner);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
    }

}
