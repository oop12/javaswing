/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author user
 */
public class ComboBoxExample1 extends JFrame implements ActionListener {
    
    JComboBox cb;
    JLabel label;
    
    ComboBoxExample1() {
        new JFrame("ComboBox Example");
        label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JButton b = new JButton("Show");
        b.setBounds(200, 100, 75, 20);
        String languages[] = {"C", "C++", "C#", "Java", "PHP"};
        cb = new JComboBox(languages);
        cb.setBounds(50, 100, 90, 20);
        b.addActionListener(this);
        add(cb);
        add(label);
        add(b);
        setSize(350, 350);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    
    public static void main(String[] args) {
        new ComboBoxExample1();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String data = "Programming language Selected: "
                + cb.getItemAt(cb.getSelectedIndex());
        label.setText(data);
    }
    
}
