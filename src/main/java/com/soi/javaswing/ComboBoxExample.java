/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.HeadlessException;
import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class ComboBoxExample extends JFrame {

    public ComboBoxExample() throws HeadlessException {
        new JFrame("ComboBox Example");
        String country[] = {"India", "Aus", "U.S.A", "England", "Newzealand"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(50, 50, 90, 20);
        add(cb);
        setSize(400, 500);
        setLayout(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new ComboBoxExample();
    }

}
