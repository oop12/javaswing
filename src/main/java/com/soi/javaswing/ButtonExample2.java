/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class ButtonExample2 {

    ButtonExample2() {
        JFrame f = new JFrame("Button Example2");
        JButton btn = new JButton(new ImageIcon("src/icon1.png"));
        btn.setBounds(100, 100, 150, 160);

        f.add(btn);

        f.setSize(300, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new ButtonExample2();
    }

}
