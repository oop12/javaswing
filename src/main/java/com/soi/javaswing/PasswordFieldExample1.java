/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class PasswordFieldExample1 {

    public static void main(String[] args) {
        JFrame f = new JFrame("Password Field Example1");
        final JLabel label = new JLabel();
        label.setBounds(20, 150, 200, 50);
        final JPasswordField value = new JPasswordField();
        value.setBounds(100, 75, 100, 30);
        JLabel lbl1 = new JLabel("Username");
        lbl1.setBounds(20,20, 80,30);  
        JLabel lbl2 = new JLabel("Password");
        lbl2.setBounds(20,75, 80,30); 
        JButton b = new JButton("Login");  
        b.setBounds(100,120, 80,30);   
        final JTextField text = new JTextField();  
        text.setBounds(100,20, 100,30);
        
        f.add(value);
        f.add(lbl1);
        f.add(label);
        f.add(lbl2);
        f.add(b);
        f.add(text);
        
        f.setSize(400, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        b.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username " + text.getText();
                data += ", Password: " + new String(value.getPassword());
                label.setText(data);
            }
        });
    }

}
