/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class DisplayingImage extends Canvas {

    @Override
    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("src/kanna1.gif");
        g.drawImage(i, 2, 2, this);

    }

    public static void main(String[] args) {
        DisplayingImage m = new DisplayingImage();
        JFrame f = new JFrame();
        Image icon = Toolkit.getDefaultToolkit().getImage("src/icon3.png");
        f.setIconImage(icon);
        f.add(m);
        f.setSize(500, 300);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
