/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author user
 */
public class LabelExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Label Example");
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lbl1 = new JLabel("Hello");
        lbl1.setBounds(100, 50, 100, 30);
        f.add(lbl1);

        JLabel lbl2 = new JLabel("World!!!");
        lbl2.setBounds(95, 100, 120, 30);
        f.add(lbl2);

    }

}
