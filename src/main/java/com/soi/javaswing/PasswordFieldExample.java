/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author user
 */
public class PasswordFieldExample {
    
    public static void main(String[] args) {
        JFrame f = new JFrame("Password Field Example ");
        JPasswordField value = new JPasswordField();
        value.setBounds(100, 100, 100, 30);
        JLabel lbl = new JLabel("Password :");
        lbl.setBounds(20, 100, 80, 30);
        
        f.add(value);
        f.add(lbl);
        f.setSize(400, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    
}
