/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author user
 */
public class TextAreaExample1 implements ActionListener {

    JLabel lbl1, lbl2;
    JTextArea area;
    JButton btn;

    TextAreaExample1() {
        JFrame f = new JFrame();
        lbl1 = new JLabel();
        lbl1.setBounds(50, 25, 100, 30);
        lbl2 = new JLabel();
        lbl2.setBounds(160, 25, 100, 30);

        area = new JTextArea();
        area.setBounds(20, 75, 250, 200);

        btn = new JButton("Count words");
        btn.setBounds(100, 300, 120, 30);
        btn.addActionListener(this);

        f.add(lbl1);
        f.add(lbl2);
        f.add(area);
        f.add(btn);

        f.setSize(450, 450);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = area.getText();
        String words[] = text.split("\\s");
        lbl1.setText("Worlds : " + words.length);
        lbl2.setText("Characters : " + text.length());
    }

    public static void main(String[] args) {
        new TextAreaExample1();
    }

}
