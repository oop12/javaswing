/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class TextFieldExample1 implements ActionListener {

    JTextField txt1, txt2, txt3;
    JButton b1, b2;

    TextFieldExample1() {
        JFrame f = new JFrame();
        txt1 = new JTextField();
        txt1.setBounds(50, 50, 150, 20);
        txt2 = new JTextField();
        txt2.setBounds(50, 100, 150, 20);
        txt3 = new JTextField();
        txt3.setBounds(50, 150, 150, 20);
        txt3.setEditable(false);

        b1 = new JButton("+");
        b1.setBounds(50, 200, 50, 50);
        b2 = new JButton("-");
        b2.setBounds(120, 200, 50, 50);
        b1.addActionListener(this);
        b2.addActionListener(this);

        f.add(txt1);
        f.add(txt2);
        f.add(txt3);
        f.add(b1);
        f.add(b2);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s1 = txt1.getText();
        String s2 = txt1.getText();

        int a = Integer.parseInt(s1);
        int b = Integer.parseInt(s2);
        int c = 0;

        if (e.getSource() == b1) {
            c = a + b;
        } else if (e.getSource() == b2) {
            c = a - b;
        }
        String result = String.valueOf(c);
        txt3.setText(result);
    }

    public static void main(String[] args) {
        new TextFieldExample1();
    }
}
