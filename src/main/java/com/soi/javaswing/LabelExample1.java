/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class LabelExample1 extends JFrame implements ActionListener {

    JTextField txt;
    JLabel lbl;
    JButton btn;

    public LabelExample1() {
        txt = new JTextField();
        txt.setBounds(50, 50, 150, 20);
        add(txt);

        lbl = new JLabel();
        lbl.setBounds(50, 100, 250, 20);
        add(lbl);

        btn = new JButton("Find ID");
        btn.setBounds(50, 150, 95, 30);
        btn.addActionListener(this);
        add(btn);

        setSize(400, 400);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String id = txt.getText();
            lbl.setText("Your ID : " + id +" Name : .......");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static void main(String[] args) {
        new LabelExample1();
    }

}
