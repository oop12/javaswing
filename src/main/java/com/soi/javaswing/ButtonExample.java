/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class ButtonExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Button Example");
        JButton b = new JButton("Click Click");
        b.setBounds(150, 100, 95, 30);
        
        f.add(b);
        f.setSize(400, 300);
        f.setLayout(null);
        f.setVisible(true);
    }
    
}
