/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.javaswing;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class TextFieldExample {

    public static void main(String[] args) {

        JFrame f = new JFrame("TextField Example");
        JTextField txt1, txt2;

        txt1 = new JTextField("Input first name");
        txt1.setBounds(50, 100, 200, 30);

        txt2 = new JTextField("Input last name");
        txt2.setBounds(50, 150, 200, 30);

        f.add(txt1);
        f.add(txt2);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
